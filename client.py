import socket
import argparse


def main():
    parser = parse_args()
    querysock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    querysock.settimeout(2)
    addr = 'localhost', 533
    querysock.connect(addr)
    querysock.sendto(bytes(parser.type, encoding='utf-8') + b'\x00' + bytes(parser.name, encoding='utf-8'), addr)
    while True:
        try:
            answer, addr = querysock.recvfrom(1024)
            print(str(answer, encoding='utf-8'))
        except socket.timeout:
            break


def parse_args():
    parser = argparse.ArgumentParser(description="simple client for my simple DNS Server")
    parser.add_argument('name', help='domain name', type=str)
    parser.add_argument('-t', '--type', help='query type', type=str, choices=['A', 'NS'], default='A')
    return parser.parse_args()


if __name__ == '__main__':
    main()
