import socket
import dnslib
import pickle
import struct
import random
import time
import copy


def main():
    forwarders = ['8.8.8.8', '212.193.163.6', '8.8.4.4']
    forw_id = 0
    forwarder = forwarders[forw_id]
    prepare_cache()
    port = 533
    types = {'A': 1, 'NS': 2}
    serversock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    serversock.settimeout(3)
    host = 'localhost'
    serversock.bind((host, port))
    print('DNS-server is bound on {}, port {}'.format(host, port))
    while True:
        try:
            full_query, fromaddr = serversock.recvfrom(1024)
            query_type = str(full_query.split(b'\x00')[0], encoding='utf-8')
            query = full_query.split(b'\x00')[1]
            print(f'from {fromaddr}:')
            print('\trecieved query: ' + str(query, encoding='utf-8') + f' for type {query_type}')
            serversock.sendto(bytes('Your request ({}) is being processed...'.format(query), encoding='utf-8'),
                              fromaddr)
            answer = find_in_cache(str(full_query, encoding='utf-8'))
            if answer is not None:
                serversock.sendto(b'Getting answer from cache: ', fromaddr)
                serversock.sendto(bytes(answer, encoding='utf-8'), fromaddr)
                print('\tgiving answer from cache')
                print('\tprocessed successfully\n')
                continue
            packet = DnsPacket()
            pk = packet.build(query, types[query_type])

            while True:
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as forwsock:
                    try:
                        # forwarder = '212.193.163.6', 53
                        forwsock.connect((forwarder, 53))
                        forwsock.sendto(pk, (forwarder, 53))
                        forwsock.settimeout(3)
                        data, addr = forwsock.recvfrom(1024)
                        answ_packet = parse_from_bytes(data)
                        answer = str(answ_packet).split('ANSWER SECTION:\n')[1]
                        dump_to_cache(str(full_query, encoding='utf-8'), answer)
                        serversock.sendto(bytes(answer, encoding='utf-8'), fromaddr)
                        print('\tprocessed successfully\n')
                        break
                    except Exception as e:
                        serversock.sendto(b'Something went wrong.', fromaddr)
                        if forw_id == len(forwarders) - 1:
                            serversock.sendto(b'Check your internet connection and the correctness of query.', fromaddr)
                            break
                        else:
                            forw_id += 1
                            forwarder = forwarders[forw_id]
                            serversock.sendto(b'Trying to ask another forwarder...', fromaddr)
                            continue
                        print(f'\tcaught exception: {e}\n')

        except socket.timeout:
            continue


def prepare_cache():
    try:
        data = pickle.load(open('cache.pickle', 'rb+'))
    except:
        return
    new_data = copy.copy(data)
    for query in data:
        if float(data[query][0]) < time.time():
            new_data.pop(query)


def find_in_cache(full_query):
    try:
        data = pickle.load(open('cache.pickle', 'rb'))
    except:
        return
    if full_query in data:
        return data[full_query][1]
    return


def dump_to_cache(full_query, answer):
    try:
        data = pickle.load(open('cache.pickle', 'rb'))
    except:
        data = {}
    ttl = int(answer.split()[1])
    expire_time = time.time() + ttl
    data[full_query] = (expire_time, answer)
    pickle.dump(data, open('cache.pickle', 'wb'))


def parseshort(bytes_data):
    return struct.unpack(">H", bytes_data)[0]


def parse_from_bytes(bytes_data):
    packet = dnslib.DNSRecord.parse(bytes_data)
    return packet


def to_name(hostname):
    name, token, index = b"", b"", 0
    if hostname and hostname[-1] != ".":
        hostname += "."
    for c in hostname:
        if c == ".":
            name += bytes([index]) + token
            token = b""
            index = 0
            continue
        else:
            token += c.encode()
            index += 1
    name += b"\x00"
    return name


class DnsPacket:
    def __init__(self):
        self.id = random.randint(1, 2 ** 16)
        self.qr = 0
        self.opcode = 0
        self.aa = 0
        self.tc = 0
        self.rd = 1
        self.ra = 0
        self.reserved = 0
        self.rcode = 0
        self.questions_count = 1
        self.answers_count = 0
        self.authority_records = 0
        self.additional_records = 0

    def build(self, name, type):
        packet = struct.pack(">H", self.id)
        flags_byte1 = int(f"{self.qr}{str(self.opcode).zfill(4)}{self.aa}{self.tc}{self.rd}").to_bytes(1, "big")
        flags_byte2 = int(f"{self.ra}{str(self.reserved).zfill(3)}{str(self.rcode).zfill(4)}").to_bytes(1, "big")
        packet += flags_byte1
        packet += flags_byte2
        packet += struct.pack(">H", self.questions_count)
        packet += struct.pack(">H", self.answers_count)
        packet += struct.pack(">H", self.authority_records)
        packet += struct.pack(">H", self.additional_records)
        packet += to_name(str(name, encoding='utf-8'))
        packet_type = type  # for A
        packet += int(str(packet_type).zfill(4)).to_bytes(2, "big")
        packet += b'\x00\x01'  # IN
        return packet


if __name__ == '__main__':
    main()
